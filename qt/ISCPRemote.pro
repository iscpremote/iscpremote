#-------------------------------------------------
#
# Project created by QtCreator 2010-03-19T20:10:57
#
#-------------------------------------------------

QT       += network

TARGET    = ISCPRemote
TEMPLATE = app

SOURCES += main.cpp \
	iscp.cpp \
	mainwindow.cpp \
	connection.cpp \
	preferencedialog.cpp \
	settings.cpp \
	config.cpp \
    commandlistedit.cpp

HEADERS += \
	iscp.h \
	mainwindow.h \
	connection.h \
	preferencedialog.h \
	config.h \
	settings.h \
    commandlistedit.h

FORMS += \
	mainwindow.ui \
	preferencedialog.ui \
    commandlistedit.ui
