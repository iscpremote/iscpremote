/**
* ISCP Remote tools collection
* URL: http://gitorious.org/iscpremote/iscpremote
* Copyright (C) 2010, Simon Schaefer (Simon.Schaefer@koeln.de)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iscp.h>
#include <config.h>
#include <preferencedialog.h>

#include <QDateTime>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	err_(stderr),
	con_(this),
	currentVolume_(0),
	settings_("Qt ISCP Remote"),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	reloadSettings();

	connect (ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));
	connect (ui->actionConnect, SIGNAL(triggered()), this, SLOT(showConnectionDialog()));
	connect (ui->actionApplicationSettings, SIGNAL(triggered()), this, SLOT(showPreferenceDialog()));

	connect (&con_, SIGNAL(newMessage(QByteArray)), this, SLOT(newMessage(QByteArray)));
	connect (&con_, SIGNAL(connected()), this, SLOT(connected()));

	foreach (const QString & cmd, commands_.keys()) {
		ui->cbCommand->addItem(commands_[cmd], cmd);
	}
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
	QMainWindow::changeEvent(e);
	switch (e->type()) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}

void MainWindow::reloadSettings()
{
	commands_ = ISCP::readCommandListFromFile(settings_.getCommandListPath());
}

void MainWindow::showPreferenceDialog()
{
	PreferenceDialog dlg;

	dlg.getPreferences(settings_);

	reloadSettings();
}

void MainWindow::showConnectionDialog()
{
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	con_.disconnectFromHost();
	con_.connectToHost(settings_.getReceiverHostname(), settings_.getReceiverPort());
	bool connected = con_.waitForConnected();
	QApplication::restoreOverrideCursor();

	if(!connected) {
		ui->statusbar->showMessage(tr("Not connected!"));
		QMessageBox::warning(this, tr("Error"), con_.errorString());
		err_ << con_.errorString();
	} else {
		ui->statusbar->showMessage(tr("Connected!"));
	}
}

void MainWindow::newMessage(const QByteArray & message)
{
	Data convertedMessage = ISCP::parseReceivedPackage(message);

//	check for special commands:
	if (convertedMessage.command.startsWith("PWR")) {
		if (convertedMessage.command.startsWith("PWR00")) {
			ui->lblOnkyoPowerStatus->setText("OFF");
		} else if (convertedMessage.command.startsWith("PWR01")) {
			ui->lblOnkyoPowerStatus->setText("ON");
		} //ignore the rest;
	}

	if (convertedMessage.command.startsWith("LMD")) {
		ui->lblSourceMode->setText(commands_[QString("!1%1").arg(convertedMessage.command)]);
	}

	if (convertedMessage.command.startsWith("MVL")) {
		bool ok;
		currentVolume_ = convertedMessage.command.mid(3).toInt(&ok, 16);
		ui->lblVolume->setText(QString::number(currentVolume_));
	}

	statusLog(QString("Received: Hex: " + message.toHex() + ", Message: " + convertedMessage.toString() + "\n"));
}

void MainWindow::connected()
{
	// send commands to get stati
	con_.sendMessage(ISCP::generateCommandPackage("!1PWRQSTN", 0)); // power status
	con_.sendMessage(ISCP::generateCommandPackage("!1LMDQSTN", 0)); // in which mode are we?
	con_.sendMessage(ISCP::generateCommandPackage("!1MVLQSTN", 0)); // master volume
}

void MainWindow::on_pbExecute_clicked()
{
	QString command = ui->cbCommand->itemData(ui->cbCommand->currentIndex()).toString();

	statusLog(QString("Sending command: %1 (%2)").arg(ui->cbCommand->currentText()).arg(command));
	con_.sendMessage(ISCP::generateCommandPackage(command.toAscii(), 0 /*TODO*/));
}

void MainWindow::statusLog(const QString & message)
{
	QString displayMessage = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss:zzz   -   ")
							+ message + "\n";

	ui->teMessages->moveCursor(QTextCursor::Start);
	ui->teMessages->insertPlainText(displayMessage);
}

void MainWindow::on_pbVolumeUp_clicked()
{
	statusLog(QString("Sending command: %1").arg("!1MVLUP"));
	con_.sendMessage(ISCP::generateCommandPackage("!1MVLUP", 0));
}

void MainWindow::on_pbVolumeDown_clicked()
{
	statusLog(QString("Sending command: %1").arg("!1MVLDOWN"));
	con_.sendMessage(ISCP::generateCommandPackage("!1MVLDOWN", 0));
}
