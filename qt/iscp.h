/**
* ISCP Remote tools collection
* URL: http://gitorious.org/iscpremote/iscpremote
* Copyright (C) 2010, Simon Schaefer (Simon.Schaefer@koeln.de)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>
*/

#pragma once

#include <QByteArray>

#include <QString>
#include <QMap>

class Header
{
public:
	QByteArray ident;
	int        headerSize;
	int        dataSize;
	char       version;

	QString toString()
	{
		return QString("HeaderSize: %2, dataSize: %3, version: %4")
				.arg(headerSize).arg(dataSize).arg((int)version);
	}
};

class Data
{
public:
	QChar unit;
	QString command;

	QString toString() {
		return QString("Data:  Unit: %1, Command: %2").arg(unit).arg(command);
	}
};

typedef QMap<QString, QString> Commands;

namespace ISCP {

QByteArray generateCommandPackage(const QString & command, int receiver);
Header readHeader(QByteArray & header);
Data parseReceivedPackage(const QByteArray & answer);
Commands readCommandListFromFile(const QString & filename);

}
