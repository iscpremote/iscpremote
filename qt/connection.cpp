/**
* ISCP Remote tools collection
* URL: http://gitorious.org/iscpremote/iscpremote
* Copyright (C) 2010, Simon Schaefer (Simon.Schaefer@koeln.de)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>
*/

#include "connection.h"

#include <QtNetwork>

#include "iscp.h"

Connection::Connection(QObject *parent) :
	QTcpSocket(parent)
{
	QObject::connect(this, SIGNAL(readyRead()), this, SLOT(processReadyRead()));
}

bool Connection::sendMessage(const QByteArray & message)
{
	if (message.isEmpty()) return false;

	return write(message) == message.size();
}

void Connection::processReadyRead()
{
	QByteArray dataRead;
	QByteArray headerRead;
	// read 16 byte for the header
	headerRead = read(16);
	if (headerRead.size() != 16) return;

	// parse header
	Header header = ISCP::readHeader(headerRead);

	// read n byte like written in header
	dataRead = read(header.dataSize);
	if (dataRead.size() != header.dataSize) return;

	emit newMessage(dataRead);
}
