/**
* ISCP Remote tools collection
* URL: http://gitorious.org/iscpremote/iscpremote
* Copyright (C) 2010, Simon Schaefer (Simon.Schaefer@koeln.de)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>
*/

#pragma once

#include <iscp.h>

#include <QTextStream>
#include <QtGui/QMainWindow>

#include <settings.h>
#include <connection.h>

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow {
	Q_OBJECT
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void showConnectionDialog();
	void showPreferenceDialog();
	void newMessage(const QByteArray & message);
	void connected();

	void on_pbExecute_clicked();
	void on_pbVolumeUp_clicked();
	void on_pbVolumeDown_clicked();

protected:
	void changeEvent(QEvent *e);

private:
	void statusLog(const QString & message);
	void reloadSettings();

private:
	QTextStream err_;
	Connection  con_;
	Commands    commands_;
	int         currentVolume_;
	Settings    settings_;

private:
	Ui::MainWindow *ui;
};
