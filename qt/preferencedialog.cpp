/**
* ISCP Remote tools collection
* URL: http://gitorious.org/iscpremote/iscpremote
* Copyright (C) 2010, Simon Schaefer (Simon.Schaefer@koeln.de)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>
*/

#include "preferencedialog.h"

#include <QFileDialog>
#include <QMessageBox>

#include "settings.h"
#include "commandlistedit.h"

#include "ui_preferencedialog.h"

PreferenceDialog::PreferenceDialog(QWidget * parent) :
	QWidget(parent),
	ui(new Ui::PreferenceDialog),
	dialog_(new QDialog(parent))
{
	ui->setupUi(dialog_);

	connect(ui->pbCommandListFileDialog, SIGNAL(clicked()), this, SLOT(on_pbCommandListFileDialog_clicked()));
}

PreferenceDialog::~PreferenceDialog()
{
	delete ui;
	delete dialog_;
}

void PreferenceDialog::getPreferences(Settings & settings)
{
	ui->leCommandList->setText(settings.getCommandListPath());
	ui->leHostname->setText(settings.getReceiverHostname());
	ui->sbPort->setValue(settings.getReceiverPort());

	if (dialog_->exec() != QDialog::Accepted) return;

	settings.setCommandListPath(ui->leCommandList->text());
	settings.setReceiverHostname(ui->leHostname->text());
	settings.setReceiverPort(ui->sbPort->value());
}

void PreferenceDialog::on_pbCommandListFileDialog_clicked()
{
	QString currentPath = ui->leCommandList->text();

	QString commandListPath = QFileDialog::getOpenFileName(this, tr("Select a command list"), currentPath);

	if (commandListPath.isEmpty()) return;

	ui->leCommandList->setText(commandListPath);
}

#include <qdebug.h>
void PreferenceDialog::on_pbEditCommandList_clicked()
{
	if (ui->leCommandList->text().isEmpty()) {
		QMessageBox::warning(this, tr("No command list path set"), tr("You have to set a command list path."));
		return ;
	}

qDebug() << "blub";
	CommandListEdit cle(this);
	cle.show(ui->leCommandList->text());
}
