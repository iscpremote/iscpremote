/**
* ISCP Remote tools collection
* URL: http://gitorious.org/iscpremote/iscpremote
* Copyright (C) 2010, Simon Schaefer (Simon.Schaefer@koeln.de)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>
*/

#pragma once

#include <QtGui/QDialog>
#include <QWidget>

#include <settings.h>

namespace Ui {
	class PreferenceDialog;
}

class PreferenceDialog : QWidget
{
	Q_OBJECT

public:
	explicit PreferenceDialog(QWidget *parent = 0);
	~PreferenceDialog();

	void getPreferences(Settings & settings);
	void setSettings(const Settings & settings);

private slots:
	void on_pbCommandListFileDialog_clicked();
	void on_pbEditCommandList_clicked();

private:
	Ui::PreferenceDialog * ui;
	QDialog *              dialog_;
};
