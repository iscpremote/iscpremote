/**
* ISCP Remote tools collection
* URL: http://gitorious.org/iscpremote/iscpremote
* Copyright (C) 2010, Simon Schaefer (Simon.Schaefer@koeln.de)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>
*/

#include "commandlistedit.h"
#include "iscp.h"

#include "ui_commandlistedit.h"

CommandListEdit::CommandListEdit(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::CommandListWidget)
{
	ui->setupUi(this);

}

void CommandListEdit::show(const QString & commandListPath)
{
	Commands commands = ISCP::readCommandListFromFile(commandListPath);
	foreach(const QString & cmd, commands.keys()) {
		QTableWidgetItem * cmdItem = new QTableWidgetItem(cmd);
		QTableWidgetItem * descItem = new QTableWidgetItem(commands[cmd]);
		ui->tblCommands->setItem(0, 0, cmdItem);
		ui->tblCommands->setItem(0, 1, descItem);
	}

	if (exec() != QDialog::Accepted) return;

	// create list to store within the file
	// overwrite commandListPath with the content of the list
}
