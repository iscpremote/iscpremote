/**
* ISCP Remote tools collection
* URL: http://gitorious.org/iscpremote/iscpremote
* Copyright (C) 2010, Simon Schaefer (Simon.Schaefer@koeln.de)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>
*/

#include "settings.h"

#include <config.h>

Settings::Settings(const QString & application) :
	QSettings(QSettings::NativeFormat, QSettings::UserScope, "iscpremote", application)
{
}

QString Settings::getCommandListPath ()
{
	return value(Config::getCommandListSettingsKeyString(), "./sortedBasicCommandsList.csv").toString();
}

void Settings::setCommandListPath(const QString & path)
{
	setValue(Config::getCommandListSettingsKeyString(), path);
}

QString Settings::getReceiverHostname ()
{
	return value(Config::getReceiverHostnameSettingsKeyString(), "192.168.0.2").toString();
}

void Settings::setReceiverHostname(const QString & name)
{
	setValue(Config::getReceiverHostnameSettingsKeyString(), name);
}

int Settings::getReceiverPort ()
{
	return value(Config::getReceiverPortSettingsKeyString(), 60128).toInt();
}

void Settings::setReceiverPort (int port)
{
	setValue(Config::getReceiverPortSettingsKeyString(), port);
}
