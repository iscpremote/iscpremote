/**
* ISCP Remote tools collection
* URL: http://gitorious.org/iscpremote/iscpremote
* Copyright (C) 2010, Simon Schaefer (Simon.Schaefer@koeln.de)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>
*/

#include "iscp.h"

#include <QTextStream>
#include <QtEndian>
#include <QFile>
#include <QStringList>

namespace {
QByteArray intToBigEndianArray(int integer)
{
	QByteArray data;
	uchar cDest[4];
	qToBigEndian(integer, cDest);

	data.append((char *)cDest, 4);
	return data;
}

qint32 byteArrayToInt(const QByteArray & array, bool & ok)
{
	if (array.size() > 4) {
		ok = false;
		return 0;
	}

	qint32 integer = 0;
	for (int i = 0; i < array.size(); i++) {
		integer |= integer | array[i];
		integer = integer << 8 * (array.size()-1 - i);
	}

	ok = true;
	return integer;
}
}


namespace ISCP {

QByteArray generateCommandPackage(const QString & command, int /*receiver*/)
{
	/* eISCP Header:
	 * from onkyo:
	 *  |  0  |  1  |  2  |  3  |
	 *-----------------------------
	 * Header:
	 *  |  I  |  S  |  C  |  P  |
	 *  |  Header size          |
	 *  |  Data size            |
	 *  | ver |   reserved      |
	 * Data:
	 *  |  !  |unit | 1C. | 2C. |
	 *  | 3C. | 4C. | 5C. |
	 *  |  ISCP Message   |
	 *  | CR  |
	 *
	 * header size fixed: 0x00000010
	 * ver = Version (fixed 0x01)
	 * unit: 1 = receiver
	 * ?C. = nth character of command
	*/
	QByteArray commandData = command.toAscii();
	if (commandData.size() <= 4) return QByteArray(); // data not valid if size is smaller than 4

	commandData.append('\r');

	// generate header
	int size = commandData.size();
	QByteArray message;
	message.append("ISCP");
	message.append(intToBigEndianArray(16));
	message.append(intToBigEndianArray(size));
	message.append((char)1).append((char)0).append((char)0).append((char)0);
	message.append(commandData);

	return message;
}

Header readHeader(QByteArray & header)
{
	bool ok = false;
	Header head;
	head.ident = header.left(4);
	head.headerSize = byteArrayToInt(header.mid(4, 4), ok);
	head.dataSize   = byteArrayToInt(header.mid(8, 4), ok);
	head.version    = byteArrayToInt(header.mid(12, 1), ok);

	return head;
}

Data parseReceivedPackage(const QByteArray & answer)
{
	Data data;
	// than
	// first char is an !
	// second one is the Unit (as string)
	// 3-5 the command
	// Payload
	data.unit    = QChar(answer[1]);
	data.command = QString(answer.mid(2, 5));

	return data;
}

Commands readCommandListFromFile(const QString & filename)
{
	Commands commands;
	QFile file(filename);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return commands;

	QTextStream in(&file);
	while (!in.atEnd()) {
		QString line = in.readLine();
		QStringList split = line.split(",", QString::SkipEmptyParts);
		if(split.size() >= 2) {
			commands[split[0]] = split[1];
		}
	}

	return commands;
}

}
