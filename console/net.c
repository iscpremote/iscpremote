/**
* ISCP Remote tools collection
* URL: http://gitorious.org/iscpremote/iscpremote
* Copyright (C) 2010, Simon Schaefer (Simon.Schaefer@koeln.de)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>
*/

#define _BSD_SOURCE

#include <endian.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "net.h"

struct stHeader
{
	char iscp[4];
	int headerSize;
	int commandSize;
	int versionPlusReserved;
};

/**
* @brief create an ISCP Paket from the given command
*
* @param command The command to create the ISCP paket from
* @param length  The length of the resulting command
* @return The created command
*/
char * createIscpPaket(char * command, int * length)
{
	if (command[0] == '\\') {
		// if the command starts with an ! than on linux it has to be ascaped with an \ so we have to remove it
		command ++;
	}

	int  commandLength = strlen(command); //
	if (commandLength < 4) return NULL;
	*length = 16 + commandLength + 1; // ISCP header has a size of 16,  +1 for ending \r
	char * paket = (char *) calloc(*length , sizeof(char));

	if(!paket) {
		*length = 0;
		return NULL;
	}

	int fixedHeaderSize = htobe32(16);
	int beCommandLength = htobe32(commandLength);
	char version = 0x01;
	char reserved[3] = {0, 0, 0};

	memcpy(paket, "ISCP", 4);
	memcpy(paket +  4 , &fixedHeaderSize, 4);
	memcpy(paket +  8 , &beCommandLength, 4);
	memcpy(paket + 12 , &version, 1);
	memcpy(paket + 13 , reserved, 3); // reserved
	memcpy(paket + 16 , command, commandLength);
	paket[16 + commandLength] = '\r';

	return paket;
}

void writeISCPCommand (int sd, char * iscpCommand)
{
		int length = 0;
		char * command = createIscpPaket(iscpCommand, &length);
		if (length == 0) return;

		write (sd, command, length);
		free (command);
}

char * receiveISCPAnswer (int sd)
{
	struct stHeader header;
	char *          answer;

	if (read (sd, &header, sizeof(struct stHeader)) < 0)
	{
		fprintf (stderr, "read of header failed.\n");
		return NULL;
	}

	uint32_t commandLength = be32toh(header.commandSize);

	answer = (char *) calloc (commandLength + 1, sizeof(char));

	if (read (sd, answer, commandLength) < commandLength) {
		fprintf (stderr, "read of command failed.\n");
		return NULL;
	}
	return answer;
}
