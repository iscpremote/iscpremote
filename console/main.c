/**
* ISCP Remote tools collection
* URL: http://gitorious.org/iscpremote/iscpremote
* Copyright (C) 2010, Simon Schaefer (Simon.Schaefer@koeln.de)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>
*/

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <libgen.h>
#include <stdlib.h>
#include <string.h>

#include "net.h"

#define TRUE             (1)
#define FALSE            (0)
#define DEFAULT_COMMAND  "!1PWRQSTN"

/**
* @brief print the usage for the program
*
* @param progPath  The path to the executable
*/
void usage(char * progPath)
{
	char * progName = basename(progPath);
	fprintf(stderr, "%s [options] -i <ip>\n", progName);
	fprintf(stderr, "Valid options are:\n");

	fprintf(stderr, "\t-i <ip>       IP of the receiver\n");
	fprintf(stderr, "\t-v            Verbose (print some status messages)\n");
	fprintf(stderr, "\t-w            Do not wipe away the last three bytes (the control characters)\n");
	fprintf(stderr, "\t-p <port>     Port of the receiver (default: 60128)\n");
	fprintf(stderr, "\t-c <command>  Command which you want to send (default: \"!1PWRQSTN\")\n");
	fprintf(stderr, "\t              do not forget to prepend the \"!1\" to the command.\n");

	fprintf(stderr, "\nExample (returns the power status):\n\t%s \"192.168.0.2\"\n\n", progName);
}

/**
* @brief The main function which controls the console client
*
* @param argc  The argument counter
* @param argv  The argument values given to the console client
*/
int main (int argc, char * argv[])
{
	int                 verbose  = FALSE;
	int                 wipe     = TRUE;
	int                 socketfd = -1;
	char *              ip       = NULL;
	int                 port     = 60128;
	char *              command  = NULL;
	struct sockaddr_in  localSockAddr;
	int                 choice;

	while (( choice = getopt (argc, argv, "i:vwp:c:")) != -1)
	{
		switch (choice)
		{
			case 'i':
				ip = strdup(optarg);
				break;
			case 'v':
				verbose = !verbose;
				break;
			case 'w':
				wipe = !wipe;
				break;
			case 'p':
				port = atoi (optarg);
				break;
			case 'c':
				command = strdup (optarg);
				break;
			case '?':
			case 'h':
				usage(argv[0]);
				return 0;
			default:
				usage(argv[0]);
				return 1;
		}
	}

	if (!ip) {
		fprintf(stderr, "You have to specify an IP\n");
		usage(argv[0]);
		return 1;
	}
	if (!command) {
		command = strdup(DEFAULT_COMMAND);
	}

	localSockAddr.sin_family        = AF_INET;
	localSockAddr.sin_port          = htons(port);
	localSockAddr.sin_addr.s_addr   = inet_addr(ip);

	if (verbose) printf("Connecting to %s:%d...\n", ip, port);
	if ((socketfd = socket (AF_INET, SOCK_STREAM, 0)) < 0) {
			fprintf (stderr, "ERROR: creating socket\n");
			return -1;
	}
	if (connect (socketfd, (struct sockaddr *) &localSockAddr, sizeof (localSockAddr)) < 0)	{
			fprintf (stderr, "ERROR: connecting to servant\n");
			return -1;
	}
	if (verbose) printf(" OK\n");

	if (verbose) printf("Sending command: %s...\n", command);
	writeISCPCommand (socketfd, command);
	if (verbose) printf(" OK\n");
	char * answer = receiveISCPAnswer (socketfd);

	if (wipe) {
		int answerLength = strlen (answer);
		answer = realloc(answer, answerLength -2);
		answer[answerLength - 3] = '\0';
	}

	if (answer) {
		if (verbose) printf("Got command: %s\n", answer);
		else         printf("%s\n", answer);
		free (answer);
	}

	if(ip) free(ip);
	if(command) free(command);
	close(socketfd);
	return 0;
}
