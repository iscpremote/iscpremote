/**
* ISCP Remote tools collection
* URL: http://gitorious.org/iscpremote/iscpremote
* Copyright (C) 2010, Simon Schaefer (Simon.Schaefer@koeln.de)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>
*/

#pragma once

/**
* @brief creates a ISCP command paket and writes it to the given socket
*
* @brief sd  socket descriptor of the network connection
* @param iscpCommand  The raw command that should be send (for example: !1PWRQSTN)
*/
void writeISCPCommand (int sd, char * iscpCommand);

/**
* @brief recieve an ISCP answer from the given socket
*
* @param sd  socket descriptor of the network connection
* @return The raw answer of the command that was sent out (for example: !1PWR01\032\r\n)
*/
char * receiveISCPAnswer (int sd);
